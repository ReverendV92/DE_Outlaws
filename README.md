# DE_Outlaws

This is a version of Propoganda's DE_Outlaws map for Counter-Strike: Source, but with files added for Garry's Mod usage.

### For the GMod version, please see the Steam Workshop page:

https://steamcommunity.com/sharedfiles/filedetails/?id=717876056

### A mirror is available on GarrysMods.org:

https://garrysmods.org/download/60811/de-outlaws

### For the original CS:S version, please see the GameBanana page:

https://gamebanana.com/maps/96393

### Credits:

* Propaganda @ Map
* Chizbone @ Navigation File
* Pedroleum @ Models
* Ranson @ Models
* V92 @ Map Icon, TTT spawn config
